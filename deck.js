import intro from './content/1/1-intro.mdx';
import sommaire from './content/1/1.2-sommaire.mdx';
import presentation from './content/1/1.3-presentation.mdx';
import es6 from './content/1/1.4-es6.mdx';
import chap2 from './content/2/2-presentation.mdx';
import chap3 from './content/3/3-presentation.mdx';
import chap4 from './content/4/4-presentation.mdx';
import chap5 from './content/5/5-presentation.mdx';
import chap6 from './content/6/6-presentation.mdx';
import merci from './content/7/7-merci.mdx';

export { default as theme } from './theme';

export default [
    ...intro,
    ...sommaire,
    ...presentation,
    ...es6,
    ...chap2,
    ...chap3,
    ...chap4,
    ...chap5,
    ...chap6,
    ...merci
];
