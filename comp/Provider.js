import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types';
import { updaters } from 'mdx-deck';
import { constants } from 'mdx-deck';
import { space, color, width } from 'styled-system'

const { previous, next } = updaters;
const { modes } = constants;

const Bottom = styled.div([], {
    position: 'fixed',
    left: 0,
    right: 0,
    bottom: 0,
})

const Button = styled.button([], {
    cursor: 'pointer',
    backgroundColor: 'transparent',
    border: 'none',
    appearance: 'none',
    width: '64px',
    height: 'calc(100vh - 0.2em)',
    transition: 'opacity 0.3s',
    opacity: 0,
    '&:focus': {
        outline: 'none',
        opacity: 1
    },
    '&:hover': {
        opacity: 1
    }
})
const Previous = styled(Button)([], {
    position: 'fixed',
    top: 0,
    left: 0,
    bottom: 0,
    '&:hover': {
        background: 'linear-gradient(90deg, #333 , transparent)'
    },
    '&:focus': {
        background: 'linear-gradient(90deg, #333 , transparent)'
    }
})
const Next = styled(Button)([], {
    position: 'fixed',
    top: 0,
    right: 0,
    bottom: 0,
    '&:hover': {
        background: 'linear-gradient(-90deg, #333 , transparent)'
    },
    '&:focus': {
        background: 'linear-gradient(-90deg, #333 , transparent)'
    }
})

const Flex = styled.div([], {
    display: 'flex',
    justifyContent: 'center',
    '@media print': {
        display: 'none'
    }
}, props => props.css,
    space,
    width,
    color
)

Flex.propTypes = {
    css: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.string
    ]),
    ...space.propTypes,
    ...width.propTypes,
    ...color.propTypes
}

export default class Provider extends React.Component {
    render() {
        const {
            children,
            mode,
            index,
            length,
            update,
        } = this.props

        if (mode !== modes.normal) {
            return (
                <React.Fragment>
                    {children}
                </React.Fragment>
            )
        }

        return (
            <React.Fragment>
                {children}
                <Previous
                    role='button'
                    mb={10}
                    onClick={e => {
                        update(previous)
                    }}
                />
                <Next
                    role='button'
                    mb={10}
                    onClick={e => {
                        update(next)
                    }}
                />
                <Bottom>
                    <Dots
                        mx='auto'
                        mb={0}
                        index={index}
                        length={length}
                        onClick={index => {
                            update({ index })
                        }}
                    />
                </Bottom>
            </React.Fragment>
        )
    }
};



const Dot = styled.button([], {
    appearance: 'none',
    border: 'none',
    backgroundClip: 'padding-box',
    width: 'auto',
    flexGrow: '1',
    height: '0.2em',
    color: 'inherit',
    cursor: 'pointer',
    padding: '0.2em',
    transition: 'background 0.3s',
    '&:focus': {
        outline: 'none',
        boxShadow: '0 0 0 1px'
    },
    '&:hover': {
        backgroundColor: '#0af'
    }
},
    props => ({
        backgroundColor: props.active ? 'rgba(220,220,220,0.6)' : 'rgba(50,50,50,0.3)'
    })
)
Dot.propTypes = {
    ...space.propTypes,
    ...color.propTypes
}
Dot.defaultProps = {
    m: 0,
    p: 1,
    color: 'text',
    bg: 'text',
}

export const Dots = ({
    index,
    length,
    onClick,
    ...props
}) =>
    <Flex {...props}>
        {Array.from({ length }).map((n, i) => (
            <Dot
                key={i}
                active={i <= index}
                title={'slide: ' + i}
                onClick={e => {
                    onClick(i)
                }}
            />
        ))}
    </Flex>

Dots.propTypes = {
    index: PropTypes.number.isRequired,
    length: PropTypes.number.isRequired,
    onClick: PropTypes.func
}

