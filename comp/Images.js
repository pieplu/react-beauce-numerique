import React from 'react';
import styled from 'styled-components';

export const ReactLogo = styled.img.attrs({
    src: 'images/logo-react.png',
    alt: 'Logo React.js'
})`
    animation: rotation 40s linear infinite;
    margin-bottom: 2rem;
    @keyframes rotation {
        0% { transform: rotate(0deg) }
        100% { transform: rotate(360deg) }
    }
`

export const Avatar = styled.img.attrs({
    src: 'images/avatar.jpg',
    alt: 'Avatar Alexis'
})`
    border-radius: 100%;
    width: 1em;
    height: 1em;
    font-size: 20vh
`
