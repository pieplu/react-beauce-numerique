import React from 'react'
import PropTypes from 'prop-types'
import { withDeck } from 'mdx-deck'
import { updaters } from 'mdx-deck'
import { constants } from 'mdx-deck'

const { modes } = constants;
const { setSteps } = updaters;
export default withDeck(class Appear extends React.Component {
    static propTypes = {
        children: PropTypes.node.isRequired,
        deck: PropTypes.object.isRequired
    }

    constructor(props) {
        super(props)
        const { update, index } = props.deck
        const steps = React.Children.toArray(props.children.props.children).length;

        update(setSteps(index, steps))
    }

    render() {
        const childrenBase = React.Children.toArray(this.props.children)
            .map(child => typeof child === 'string'
                ? <div>{child}</div>
                : child
            )
        const children = React.Children.toArray(this.props.children.props.children)
            .map(child => typeof child === 'string'
                ? <div>{child}</div>
                : child
            )
        const { step, mode } = this.props.deck;

        if (mode !== modes.normal) {
            return childrenBase
        }

        if (typeof window !== 'undefined' && window.navigator.userAgent.includes('Print/PDF')) {
            return childrenBase;
        }

        if (typeof window !== 'undefined' && window.matchMedia('print').matches) {
            return childrenBase;
        }

        return (
            React.cloneElement(this.props.children,
                {},
                children.filter((_, index) => (step >= index + 1)).map((child, i) => (<child.type {...child.props} yolo="a" style={{}} key={i} />))
            )
        )
    }
})
