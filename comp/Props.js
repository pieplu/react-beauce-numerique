import React from 'react';

export class PropsComponent extends React.Component {

    name = 'Elmo';
    imageUrl = 'images/elmo.png';

    render() {
        return <ChildComponent name={this.name} image={this.imageUrl} />;
    }
}

export const ChildComponent = (props) => {
    return (
        <article>
            <h1>{props.name}</h1>
            <img src={props.image} />
        </article>
    );
};
