import React from 'react';
import styled from 'styled-components';

const Button = styled.button.attrs({
    type: 'button'
})`
    cursor: pointer;
    background-color: transparent;
    color: white;
    border: 5px solid currentColor;
    font-size: 1.5rem;
    font-weight: bold;
    text-transform: uppercase;
    padding: 0.5rem;
    transition: color .5s, border .5s;
    &:hover {
        color: yellow
    }
`

const Checkbox = styled.input.attrs({
    type: 'checkbox'
})`
    cursor: pointer;
    background-color: transparent;
    color: white;
    border: 5px solid currentColor;
    font-size: 1.5rem;
    font-weight: bold;
    text-transform: uppercase;
    padding: 0.5rem;
    transition: color .5s, border .5s;
    &:hover {
        color: yellow
    }
`

export class Counter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            clickCount: 0 // Par défaut l'état du compteur est à Zéro
        };
        this._incrementClick = this._incrementClick.bind(this);
    }
    _incrementClick() {
        this.setState({
            clickCount: this.state.clickCount + 1 // Au clique ajoute 1 au compteur
        });
    }
    render() {
        return (
            <div>
                {/*Ici le h2 est mis à jour avec la nouvelle valeur*/}
                <h2>{this.state.clickCount}</h2>
                <Button onClick={this._incrementClick}> Add +</Button>
            </div>
        );
    }
}


export class LifecycleWrap extends React.PureComponent {
    state = {
        display: false
    }

    invert = () => {
        this.setState((prevState) => ({
            display: !!prevState.display
        }));
    }

    render() {
        if (this.state.display) {
            return <LifecycleEx />;
        }
        return <Checkbox value={this.state.display} onClick={this.invert} />
    }
}

export class LifecycleEx extends React.PureComponent {
    constructor() {
        super();
        this.state = {
            data: []
        };
    }
    _addItem = () => {
        const newValue = this._generateValue();
        this.setState((prevState) => ({
            data: [...prevState.data, newValue]
        }));
    }
    _generateValue() {
        return Math.floor(Math.random() * 1000) + 1;
    }
    _removeItem = (valueToRemove) => {
        this.setState((prevState) => ({
            data: prevState.data.filter((item) => {
                return item !== valueToRemove;
            })
        }));
    }

    // shouldComponentUpdate() {
    //     console.log(`Should ParentList update ?`);
    //     return true;
    // }

    componentDidUpdate() {
        console.log(`%c ParentList did update`, 'font-size: 2em; background: yellow; color: #333; padding: 5px; font-weight: bold');
    }

    render() {
        const items = this.state.data.map((value, index) => {
            return (
                <Component
                    value={value}
                    key={`${value}`}
                    index={index}
                    removeItem={this._removeItem} />
            );
        });
        return (
            <div>
                <Button onClick={this._addItem}>
                    Add one
                </Button>
                <ul>
                    {items.length > 0 ? items : 'Nothing to show'}
                </ul>
            </div>
        );
    }
}

class Component extends React.Component {

    componentDidMount() {
        console.log(`%c Item ${this.props.index} ( ${this.props.value} ) is mount.`, 'font-size: 2em; background: #eee; color: blue; padding: 5px');
    }

    componentDidUpdate() {
        console.log(`%c Item ${this.props.index} ( ${this.props.value} ) did update`, 'font-size: 2em; background: yellow; color:#333 ; padding: 5px');
    }

    componentWillUnmount() {
        console.log(`%c Item ${this.props.index} ( ${this.props.value} ) will unmount.`, 'font-size: 2em; background: #eee; color: red; padding: 5px');
    }
    render() {
        const { value, removeItem } = this.props;
        return (
            <li>{value} <Button onClick={() => removeItem(value)}>X</Button></li>
        );
    }
}
