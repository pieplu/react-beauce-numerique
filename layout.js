import React from 'react'

export const WithBgImage = (src) => ({ children }) => {
    return (
        <div style={{
            width: '100vw',
            height: '100%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            backgroundImage: `url(${src})`,
            backgroundSize: 'cover'
        }}>
            {children}
        </div>
    )
}

export const WithBgColor = (bgColor, color) => ({ children }) => {
    return (
        <div style={{
            width: '100vw',
            height: '100%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: bgColor,
            color
        }}>
            {children}
        </div>
    )
}

export const WithEcmaScript = WithBgColor('yellow', 'black');
