# Présentation React - Beauce Numérique

19 Février 2019 - [présentation](react-bn.pieplu.ca)

## Development

To run the presentation deck in development mode:

```sh
npm start
```

Edit the [`deck.js`](deck.js) file to get started.

## Exporting

To build the presentation deck as static HTML:

```sh
npm run build
```

To export a PDF:

```sh
npm run pdf
```

To export an image of the title slide:

```sh
npm run image
```

For more documentation see the [mdx-deck][] repo.

[mdx-deck]: https://github.com/jxnblk/mdx-deck
