import { condensed as theme } from 'mdx-deck/themes';
import codeSuferStyle from 'prism-react-renderer/themes/dracula';
import prismStyle from 'react-syntax-highlighter/dist/styles/prism/darcula';
import { components } from 'mdx-deck-code-surfer';
import Provider from './comp/Provider'


export default {
    ...theme,
    codeSurfer: {
        ...codeSuferStyle,
        showNumbers: false
    },
    Provider,
    prism: {
        style: prismStyle
    },
    ul: {
        ...theme.ul,
        width: '80vw'
    },
    p: {
        textAlign: 'left'
    },
    h1: {
        textAlign: 'left'
    },
    h3: {
        fontSize: '2em',
        textAlign: 'left'
    },
    h4: {
        fontSize: '1.5em',
        textAlign: 'left'
    },
    h5: {
        fontSize: '1.3em',
        textAlign: 'left'
    },
    h6: {
        fontSize: '1.1em',
        textAlign: 'left'
    },
    code: {
        ...theme.code,
        color: '#ffffaa',
        backgroundColor: '#222',
        borderRadius: '0.2em',
        padding: '0 0.2em'
    },
    components: {
        code: components.code
    }

    // Customize your presentation theme here.
    //
    // Read the docs for more info:
    // https://github.com/jxnblk/mdx-deck/blob/master/docs/theming.md
    // https://github.com/jxnblk/mdx-deck/blob/master/docs/themes.md
};
